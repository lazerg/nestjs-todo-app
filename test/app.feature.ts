import * as request from 'supertest';
import { createApp } from './createApp';

test('Test Response', async () => {
    const app = await createApp();

    return request(app.getHttpServer())
        .get('/')
        .expect(200)
        .expect('Hello World!');
});
