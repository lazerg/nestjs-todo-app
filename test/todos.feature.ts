import * as request from 'supertest';
import { createApp } from './createApp';

test('/todos/ (POST) - Create a new todo', async () => {
    const app = await createApp();

    return request(app.getHttpServer())
        .post('/todos')
        .type('form')
        .send({ name: 'New Todo' })
        .expect(201)
        .then((response) => {
            expect(response.body.id).toBe(1);
            expect(response.body.name).toBe('New Todo');
        });
});

test('/todos/ (GET) - Fetch all todos', async () => {
    const app = await createApp();

    return request(app.getHttpServer())
        .get('/todos')
        .expect(200)
        .then((response) => {
            expect(response.body).toEqual([]);
        });
});
