import { Injectable } from '@nestjs/common';
import { Todo } from './todo';

@Injectable()
export default class TodosService {
    private todos: Todo[] = [];
    private id = 0;

    all(): Todo[] {
        return this.todos;
    }

    create(todo: Todo): Todo {
        todo.id = ++this.id;
        this.todos.push(todo);
        return todo;
    }
}
