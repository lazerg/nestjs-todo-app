import { Body, Controller, Get, Post } from '@nestjs/common';
import TodosService from './todos.service';
import { Todo } from './todo';

@Controller('/todos')
export class TodosController {
    constructor(private todosService: TodosService) {}

    @Get()
    index(): Todo[] {
        return this.todosService.all();
    }

    @Post()
    store(@Body() todo: Todo): Todo {
        return this.todosService.create(todo);
    }
}
