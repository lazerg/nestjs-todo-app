FROM node:16.1.0-alpine3.13

WORKDIR /usr/src/app

COPY package.json .
COPY yarn.lock .

RUN yarn install

COPY . .

RUN yarn build
RUN yarn install --prod

CMD ["node", "dist/main"]
